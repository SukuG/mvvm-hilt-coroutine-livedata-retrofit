package com.example.suku_hiltmvvmcoroutineretrofit.repository

import com.example.suku_hiltmvvmcoroutineretrofit.model.EmployeeResponse
import com.example.suku_hiltmvvmcoroutineretrofit.network.NetworkResult
import com.example.suku_hiltmvvmcoroutineretrofit.network.RemoteDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class Repository @Inject constructor(private val remoteDataSource: RemoteDataSource) {

    //use coroutine for background thread
    suspend fun getValues(): NetworkResult<EmployeeResponse> {
        return withContext(Dispatchers.IO) {
            remoteDataSource.getEmployees()
        }
    }

    //TODO have to check below scenario
    /* // OR BELOW WAY
     fun getEmployees() = liveData(Dispatchers.IO) {
         emit(NetworkResult.Loading())
         try {
             emit(NetworkResult.Success(data = remoteDataSource.getEmployees()))
         } catch (exception: Exception) {
             emit(NetworkResult.Failure(message = exception.message ?: "Default Error Message"))
         }
     }*/
}