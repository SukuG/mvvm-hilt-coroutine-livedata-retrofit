package com.example.suku_hiltmvvmcoroutineretrofit.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.suku_hiltmvvmcoroutineretrofit.model.EmployeeResponse
import com.example.suku_hiltmvvmcoroutineretrofit.network.NetworkResult
import com.example.suku_hiltmvvmcoroutineretrofit.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    var liveDataEmployees: MutableLiveData<EmployeeResponse?> = MutableLiveData()

    fun callApi() {
        viewModelScope.launch {
            delay(5000)
            val result = repository.getValues()
            when (result) {
                is NetworkResult.Success -> {
                    liveDataEmployees.postValue(result.data)
                }
                is NetworkResult.Failure -> {
                    Log.d("ERROR", "" + result.message)
                }
                else -> {

                }
            }
        }
    }

    //TODO have to check below scenario
    /*
    fun api(): LiveData<NetworkResult<NetworkResult<EmployeeResponse>>> {
        return repository.getEmployees()
    }*/
}