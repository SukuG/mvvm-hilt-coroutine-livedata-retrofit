package com.example.suku_hiltmvvmcoroutineretrofit.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.suku_hiltmvvmcoroutineretrofit.R
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.liveDataEmployees.observe(
            this,
            Observer { Log.d("Sukumar", "DATA:" + Gson().toJson(it)) })
        mainViewModel.callApi()

        //TODO have to check below scenario
        //callAPITwo()
        //mainViewModel.api()
    }

    //TODO have to check below scenario
    /*private fun callAPITwo() {
        mainViewModel.api().observe(this, Observer {
            when (it?.data) {
                is NetworkResult.Success -> {
                    Log.d("Sukumar", "DATA:" + Gson().toJson(it.data.data))
                }
                is NetworkResult.Failure -> {
                    Log.d("ERROR", "" + it.data.message)
                }
                else -> {

                }
            }
        })

    }*/
}