package com.example.suku_hiltmvvmcoroutineretrofit.network

import com.example.suku_hiltmvvmcoroutineretrofit.model.EmployeeResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    //Deprecated from 2.6.0
    /*@GET("search/repositories")
   suspend fun getEmployeesAsync(@Query("q") query: String): Deferred<Response<EmployeeResponse>>*/

    @GET("search/repositories")
    suspend fun getEmployeesAsync(@Query("q") query: String): Response<EmployeeResponse>
}