package com.example.suku_hiltmvvmcoroutineretrofit.network

import com.example.suku_hiltmvvmcoroutineretrofit.model.EmployeeResponse
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val apiInterface: ApiInterface) :
    BaseApiResource() {

    suspend fun getEmployees(): NetworkResult<EmployeeResponse> {
        return invokeApiRequest(apiCall = { apiInterface.getEmployeesAsync("India")
        })
    }
}